public class Loop {
  public static void main(String[] args) {
    int x = 0;
    while (x < 50) {
      x = x + 2;
    }
    Wunderhorn.ensure(x != 41);
  }
}
