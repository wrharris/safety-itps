all:
	oasis setup && ocaml setup.ml -configure
	ocaml setup.ml -build

configure:
	./scripts/configure.sh

clean:
	ocaml setup.ml -clean || true
	find -name '*.mllib' -delete
	find -name '*.mldylib' -delete
	find -name '*.byte' -delete
	rm -rf bin/*
	rm -f setup.ml

test:
	./scripts/test_simple.sh run

install:
	ln -s "`pwd`/wunderhorn" /usr/local/bin/wunderhorn

.PHONY: configure all clean test install
