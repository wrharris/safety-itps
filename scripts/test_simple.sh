#!/usr/bin/env bash
SOURCE_DIR=$(readlink -f "${BASH_SOURCE[0]}")
SOURCE_DIR=$(dirname "$SOURCE_DIR")
# shellcheck source=common.sh
source "$SOURCE_DIR/common.sh"

rm -rf bin
mkdir -p bin
cp "$(source_dir)"/../main.byte bin
cd bin || exit 1

type=$1

function run {
  "$(source_dir)"/run_simple_test.sh "$1" "$type"
}

echo "Running all simple benchmarks with classpath set to: $(classpath)."

run "$(source_dir)"/../benchmark/arbitrary.fail
run "$(source_dir)"/../benchmark/assume.fail
run "$(source_dir)"/../benchmark/assume.pass
# run "$(source_dir)"/../benchmark/dispatch.fail
# run "$(source_dir)"/../benchmark/dispatch.pass
run "$(source_dir)"/../benchmark/lcm.pass
run "$(source_dir)"/../benchmark/lcm.fail
run "$(source_dir)"/../benchmark/mutableparam.fail
run "$(source_dir)"/../benchmark/slow-add.pass
run "$(source_dir)"/../benchmark/slow-add.fail
run "$(source_dir)"/../benchmark/fib.pass
run "$(source_dir)"/../benchmark/fib.fail
run "$(source_dir)"/../benchmark/mutual_recursion.pass
run "$(source_dir)"/../benchmark/bit_shift.pass
run "$(source_dir)"/../benchmark/bit_shift.fail
run "$(source_dir)"/../benchmark/static_field.fail
run "$(source_dir)"/../benchmark/static_field.pass
run "$(source_dir)"/../benchmark/object_identity.pass
run "$(source_dir)"/../benchmark/object_identity.fail
run "$(source_dir)"/../benchmark/ctor.pass
run "$(source_dir)"/../benchmark/multi_call.pass
run "$(source_dir)"/../benchmark/mixed_array.fail
run "$(source_dir)"/../benchmark/array.fail
run "$(source_dir)"/../benchmark/array.pass
run "$(source_dir)"/../benchmark/method.pass
run "$(source_dir)"/../benchmark/method.fail
run "$(source_dir)"/../benchmark/return_real.fail
run "$(source_dir)"/../benchmark/real_arith.fail
run "$(source_dir)"/../benchmark/real_arith.pass
run "$(source_dir)"/../benchmark/not_null.fail
run "$(source_dir)"/../benchmark/not_null.pass
run "$(source_dir)"/../benchmark/is_null.pass
run "$(source_dir)"/../benchmark/is_null.fail
run "$(source_dir)"/../benchmark/float.pass
run "$(source_dir)"/../benchmark/float.fail
run "$(source_dir)"/../benchmark/non_int_field.fail
run "$(source_dir)"/../benchmark/field.pass
run "$(source_dir)"/../benchmark/field.fail
run "$(source_dir)"/../benchmark/call.pass
run "$(source_dir)"/../benchmark/call.fail
run "$(source_dir)"/../benchmark/while.pass
run "$(source_dir)"/../benchmark/while.fail
