user=$(who am i | head -n1 | awk '{print $1;}') 

# must update repos before installing
apt-get update

# install ocam and other deps
# z3 Deps: python2
# javalib/sawja Deps: zlib1g-dev
# ocamlfind Deps: m4
# project Deps: openjdk-7-jdk
# scheme-format Deps: chicken-bin libchicken-dev
apt-get install -y \
  m4 \
  zlib1g-dev \
  python \
  openjdk-7-jdk \
  chicken-bin \
  libchicken-dev \
  opam \
  oasis \
  ocaml

# set up scheme-format helper binary
echo "sudo -u $user git clone 'https://github.com/russellw/scheme-format.git'"

sudo -u $user git clone 'https://github.com/russellw/scheme-format.git'
cd scheme-format
sudo -u $user git checkout f03ebe75fd4e7a5f22f40744d37d0f67c20d3bd8
sudo -u $user csc main.scm -o scheme-format

mv scheme-format /usr/local/bin/
cd ..
rm -rf scheme-format

# setup opam and install ocam libraries
sudo -u $user opam init -y
sudo -u $user opam install -y core sawja

# # build z3 with ml bindings
# # stay on version 4.5.0 for now
sudo -u $user git clone 'https://github.com/Z3Prover/z3.git'
cd z3
sudo -u $user git checkout tags/z3-4.5.0
sudo -u $user git apply --whitespace=nowarn ../z3-patches/cvoss_z3.patch
sudo -u $user python scripts/mk_make.py --ml
cd build
sudo -u $user make

make install

cd ../..
sudo -u $user rm -rf z3

# Add config to .bashrc
echo 'eval $(opam config env)' >> ~/.bashrc
